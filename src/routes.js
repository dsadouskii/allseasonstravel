import Landing from './components/pages/landing.vue'
import About from './components/pages/about.vue'
import Tours from './components/pages/tours.vue'
import Visas from './components/pages/visas.vue'

export default[
	{path: '/', component: Landing},
	{path: '/about', component: About},
	{path: '/tours', component: Tours},
	{path: '/visas', component: Visas}
]