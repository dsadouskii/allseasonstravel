import Vue from 'vue'
import Router from 'vue-router';
import App from './App.vue'
import Routes from './routes'
import Resource from 'vue-resource'
import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyD1cCE2U1eVYCYHiMtO_60ZaITj9pTejpU",
    libraries: "places",
    region: "ru,en"
  }
});

Vue.use(Resource);
Vue.use(Router);

const router = new Router({
	routes: Routes,
	mode: 'history'
});


new Vue({
  el: '#app',
  render: h => h(App),
  router: router
});
